package com.hiwin.product_service.app.data;

import com.hiwin.product_service.domain.entities.postgres.Product;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class ProductsResponse {
    private List<Product> products;
    private MetaData metaData;

    public ProductsResponse(Page<Product> page) {
        products = page.getContent();
        metaData = MetaData.of(page);
    }
}

package com.hiwin.product_service.app.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@Log4j2
@Valid
public class ProductDto {

    @NotNull(message = "name id not null")
    @Size(min = 3, max = 150)
    private String name;

    @NotNull(message = "category id not null")
    @JsonProperty("category_id")
    private int categoryId;

    @NotNull(message = "images not null")
    @NotEmpty
    @Size(max = 5)
    private String[] images;

    @NotNull(message = "description not null")
    @JsonProperty("description")
    private String description;

    @NotNull(message = "description_picking not null")
    @JsonProperty("description_picking")
    private String descriptionPicking;

    @NotNull(message = "price not null")
    @Min(value = 1000)
    private Long price;

    @Min(value = 1, message = "quantity > 1")
    @NotNull(message = "quantity not null")
    private Integer quantity;

}

package com.hiwin.product_service.app.controller;

import com.hiwin.product_service.app.data.ProductsResponse;
import com.hiwin.product_service.app.dto.ProductChangeStateDto;
import com.hiwin.product_service.app.dto.ProductDto;
import com.hiwin.product_service.domain.entities.postgres.Product;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/products")
public class ProductController extends BaseController {

    /**
     * Get product by id
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Product getById(@PathVariable long id) throws Exception {
        return productService.getById(id);
    }

    /**
     * Get all by page
     *
     * @param pageable
     * @return
     */
    @GetMapping()
    public ProductsResponse getAll(Pageable pageable) {
        Page<Product> page = productService.getAll(pageable);
        return new ProductsResponse(page);
    }

    /**
     * Create new product
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @PostMapping()
    public Product create(@Valid @RequestBody ProductDto dto) throws Exception {
        ModelMapper mapper = new ModelMapper();
        Product product = mapper.map(dto, Product.class);
        return productService.create(product);
    }

    /**
     * Update product
     *
     * @param dto
     * @param id
     * @return
     * @throws Exception
     */
    @PutMapping("/{id}")
    public Product update(@Valid @RequestBody ProductDto dto, @PathVariable long id) throws Exception {
        ModelMapper mapper = new ModelMapper();
        Product product = mapper.map(dto, Product.class);
        return productService.update(id, product);
    }


    /**
     * Change status for product
     *
     * @return
     * @throws Exception
     */
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public boolean changeState(@PathVariable long id, @Valid @RequestBody ProductChangeStateDto dto) throws Exception {
        return productService.changeState(id, dto);
    }

    /**
     * Delete product
     *
     * @return
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public boolean delete(@PathVariable long id) throws Exception {
        return productService.delete(id);
    }

    /**
     * Delete product
     *
     * @return
     * @throws Exception
     */
    @GetMapping("/clean")
    @ResponseStatus(HttpStatus.OK)
    @CacheEvict(value = "products", key = "#pageable.pageNumber")
    public void cleanCache(Pageable pageable) throws Exception {
    }
}

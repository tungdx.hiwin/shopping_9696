package com.hiwin.product_service.domain.entities.postgres;

import com.hiwin.product_service.domain.constants.ProductConstant;
import com.hiwin.product_service.domain.types.ProductState;
import com.hiwin.product_service.domain.utils.PostgreSQLEnumType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "product", schema = "products")
@TypeDef(name = "pg-enum", typeClass = PostgreSQLEnumType.class)
@TypeDef(name = "string-array", typeClass = StringArrayType.class)
public class Product implements Cloneable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "category_id")
    private int categoryId;

    @Type(type = "string-array")
    @Column(name = "images")
    private String[] images;

    @Column(name = "description")
    private String description;

    @Column(name = "description_picking")
    private String descriptionPicking;

    @Column(name = "price")
    private Long price;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "sold_quantity")
    private Integer soldQuantity;

    @Enumerated(value = EnumType.STRING)
    @Type(type = "pg-enum")
    @Column(nullable = false, columnDefinition = "product_state", name = "state")
    private ProductState state;

    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public void inferProperties() {
        LocalDateTime time = LocalDateTime.now(ProductConstant.ZONE_ID);
        state = ProductState.PENDING;
        id = null;
        soldQuantity = 0;
        createdAt = time;
        updatedAt = time;
    }
}


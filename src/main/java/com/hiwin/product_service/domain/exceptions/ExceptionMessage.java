package com.hiwin.product_service.domain.exceptions;

public class ExceptionMessage {
    public static String PRODUCT_NOT_FOUND = "Product not found";
    public static String ERROR_QUANTITY = "The amount sold must not be greater than the quantity";
}
